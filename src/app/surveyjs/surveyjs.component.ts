import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import * as Survey from 'survey-angular';
import * as widgets from "surveyjs-widgets";
import { Formulario } from 'src/app/model/Formulario';
import { setarValorQuestaoSurveyJS } from './surveyjs-util.component';
import { Router } from '@angular/router';

widgets.select2(Survey);

@Component({
    selector: 'app-surveyjs-component',
    templateUrl: './surveyjs.component.html',
    styleUrls: ['./surveyjs.component.css']
})
export class SurveyJSComponent implements OnInit, OnDestroy {

    survey: Survey.Survey;
    carregado: boolean = false;

    @Input() formulario: Formulario;
    @Input() respostas: any;

    subscriptions: any = {};

    @Output('onCompleteEventEmitter') onCompleteEventEmitter = new EventEmitter<any>();
    @Output('onCancelarEventEmitter') onCancelarEventEmitter = new EventEmitter<any>();
    @Output('onExcluirEventEmitter') onExcluirEventEmitter = new EventEmitter<any>();

    constructor(
        public snackBar: MatSnackBar,
    ) {
    }

    ngOnInit() {
        this.generateSurvey();
    }

    generateSurvey() {

        this.survey = new Survey.Model(this.formulario.surveyJS);

        //Adicionando hora atual como padrão para todos os campos do tipo time
        //TODO: Verificar essa questão
        this.survey
            .onAfterRenderQuestion
            .add(function (survey, options) {
                if (options.question.inputType === "time" && options.question.isEmpty()) {
                    var data = new Date();
                    var hora = data.getHours();
                    var minuto = data.getMinutes();
                    let horaminuto = ("0" + hora).slice(-2) + ":" + ("0" + minuto).slice(-2);
                    options.question.value = horaminuto;
                }

                if (options.question.inputType === "date" && options.question.isEmpty()) {
                    options.question.value = new Date().toISOString().substring(0, 10);
                }

            });

        this.survey.locale = 'pt';
        this.survey.completeText = 'Atualizar'

        this.survey.showNavigationButtons = false;

        this.survey.completedHtml = "Tarefa executada com sucesso"
        this.survey.onComplete.add(result => {
            this.salvarRespostas(result.data)
        });

        Survey
            .StylesManager
            .applyTheme("darkblue");
        Survey.SurveyNG.render("surveyElement", {
            model: this.survey,
            onCurrentPageChanged: this.executarAoTrocarPagina
        });

        this.executarAoTrocarPagina(this.survey);

        if (this.respostas) {
            console.log('respostas no componente survey',this.respostas );
            this.survey.data = this.respostas;
        }

    }

    executarAoTrocarPagina(survey) {
        let surveyPrevButton = document
            .getElementById('surveyPrev');
        if (surveyPrevButton) {
            surveyPrevButton.style
                .display = !survey.isFirstPage
                    ? "inline"
                    : "none";
        }

        let surveyNextButton = document
            .getElementById('surveyNext');
        if (surveyNextButton) {
            surveyNextButton.style
                .display = !survey.isLastPage
                    ? "inline"
                    : "none";
        }
        let surveyCompleteButton = document
            .getElementById('surveyComplete');
        if (surveyCompleteButton) {
            surveyCompleteButton.style
                .display = survey.isLastPage
                    ? "inline"
                    : "none";
        }
    }

    salvarRespostas(respostas: any) {
        this.onCompleteEventEmitter.emit(respostas);
    }

    concluir() {
        this.survey.completeLastPage();
        let dataCopy = this.survey.data;
        this.survey.clear();
        this.survey.data = dataCopy
        this.survey.render();
    }

    excluir() {
        this.onExcluirEventEmitter.emit();
    }

    cancelar() {
        this.onCancelarEventEmitter.emit();
    }


    ngOnDestroy(): void {
        for (var key in this.subscriptions) {
            if (this.subscriptions.hasOwnProperty(key)) {
                this.subscriptions[key].unsubscribe();
            }
        }
    }

    abrirSnackBar(message: string, action: string, classe: string) {
        this.snackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }


}
