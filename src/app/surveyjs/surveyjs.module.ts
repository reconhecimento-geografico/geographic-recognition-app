import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule, MatIconModule, MatMenuModule, MatButtonModule } from '@angular/material';
import { SurveyJSComponent } from './surveyjs.component';

@NgModule({
  declarations: [SurveyJSComponent],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule
  ],
  exports: [SurveyJSComponent]
})
export class SurveyJSModule { }
