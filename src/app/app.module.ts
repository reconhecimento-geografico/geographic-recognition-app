import { ListaDeTrabalhoLocalComponent } from './lista-de-trabalho/lista-de-trabalho-local.component';
import { ListaDeTrabalhoNuvemComponent } from './lista-de-trabalho/lista-de-trabalho-nuvem.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ListaDeTrabalhoComponent } from './lista-de-trabalho/lista-de-trabalho.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTabsModule, MatToolbarModule, MatFormFieldModule, MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatButtonModule, MatIconModule, MatCheckboxModule, MatSnackBarModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SurveyJSModule } from './surveyjs/surveyjs.module';
import { ListaDeTrabalhoImoveisComponent } from './lista-de-trabalho/lista-de-trabalho-imoveis.component';
import { ListaDeTrabalhoImoveisFormularioComponent } from './lista-de-trabalho/lista-de-trabalho-imoveis-formulario.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
@NgModule({
  declarations: [
    AppComponent,
    ListaDeTrabalhoComponent,
    ListaDeTrabalhoNuvemComponent,
    ListaDeTrabalhoLocalComponent,
    ListaDeTrabalhoImoveisComponent,
    ListaDeTrabalhoImoveisFormularioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    MatTabsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    MatSnackBarModule,
    SurveyJSModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
