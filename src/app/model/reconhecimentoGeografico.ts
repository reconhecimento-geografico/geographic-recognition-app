import { FormGroup } from '@angular/forms';
import { Formulario } from './Formulario';

export default class ReconhecimentoGeografico{
    id : string;
    titulo  : string;
    conteudo : any;
    formulario : Formulario;
   

    public static popularBaseadoEmFormGroup(registro: ReconhecimentoGeografico, formGroup: FormGroup) {
        registro.titulo =  formGroup.value.titulo;
        registro.conteudo =  formGroup.value.conteudo;
        registro.formulario =  formGroup.value.formulario;
       
        
    }
}
