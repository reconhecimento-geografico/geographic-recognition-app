import { Questao } from "./Questao";

export class Formulario {
    id: string;
    titulo: string;
    descricao: string;
    questoesExibidasNaPesquisa: Questao[];    
    formulariosFilhos: Formulario[];
    surveyJS: any;

    public static gerarDescricao(formulario:Formulario): string {
        let jsonSurvey = JSON.parse(JSON.stringify(formulario.surveyJS));
        if(jsonSurvey != null && jsonSurvey.title){
            if(jsonSurvey.title.pt){
                return jsonSurvey.title.pt;
            }
            return jsonSurvey.title;
        }
        return formulario.titulo;
    }
}