
import { FormGroup } from '@angular/forms';
import TipoImovel from './tipoImovel';
import Territorio from './territorio';

export default class Imovel{
    id : number;
    tipoImovel  : TipoImovel;
    pontoEstrategico : string;
    territorio  : Territorio;
    logradouro : string;
    cep : string;
    numero  : string;
    complemento  : string;
    sequencia : string;
    numQuarteirao : string;
    ladoQuarteirao : string;
    poligono : string[][];

    public static popularBaseadoEmFormGroup(registro: Imovel, formGroup: FormGroup) {
        registro.tipoImovel =  formGroup.value.tipoImovel;
        registro.pontoEstrategico =  formGroup.value.pontoEstrategico;
        registro.territorio =  formGroup.value.territorio;
        registro.logradouro =  formGroup.value.logradouro;
        registro.cep =  formGroup.value.cep;
        registro.numero =  formGroup.value.numero;
        registro.complemento =  formGroup.value.complemento;
        registro.sequencia =  formGroup.value.sequencia;
        registro.numQuarteirao =  formGroup.value.numQuarteirao;
        registro.ladoQuarteirao =  formGroup.value.ladoQuarteirao;
        registro.poligono =  JSON.parse(formGroup.value.poligono);
  
    }
}
