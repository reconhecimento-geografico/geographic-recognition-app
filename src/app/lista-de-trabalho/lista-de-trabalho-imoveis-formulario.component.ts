import { element } from 'protractor';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { HttpListaDeTrabalhoService } from './service/http-lista-de-trabalho.service';
import ListaDeTrabalho from '../model/listaDeTrabalho';
import { merge, of as observableOf, pipe } from 'rxjs';
import { startWith, switchMap, map, catchError, debounceTime, tap, finalize } from 'rxjs/operators';
import { Page } from '../model/Page';
import { SelectionModel } from '@angular/cdk/collections';
import ItemLista from '../model/itemLista';
import { HttpItemListaService } from './service/http-item-lista.service';
import { ListaLocalService } from './service/lista-local-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-lista-de-trabalho-imoveis-formulario',
  templateUrl: './lista-de-trabalho-imoveis-formulario.component.html',
  styleUrls: ['./lista-de-trabalho.component.scss']
})
export class ListaDeTrabalhoImoveisFormularioComponent implements OnInit {
  //Atributos para selecao de filtro por imovel Pai
  formGroup: FormGroup = this.formBuilder.group({
    "imovel" : new FormControl("")
  });

  id: number;
  listaId: number;
  itemLista = ItemLista;
  item : ItemLista;

  constructor(
    private location : Location,
    private apiListaDeTrabalhoService: HttpListaDeTrabalhoService,
    private formBuilder: FormBuilder,
    private listaLocalService: ListaLocalService,
    private apiItemListaService: HttpItemListaService,
    private snackBar: MatSnackBar,
    private router : Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.activatedRoute.params.subscribe(params => {
    this.id = params.id;
    this.listaId = params.listaId;
    });
  }

  ngOnInit() {
    this.pesquisarImovel()

  }

  pesquisarImovel() {
    this.formGroup
      .get('imovel')
      .valueChanges
      .pipe(
        debounceTime(300),
        switchMap(value => this.listaLocalService.getImoveisByListaId(this.listaId, null, null, null, null)
        )
      )
      .subscribe(resultado => {
        let id = this.id;
        if(resultado[0].itens)
        this.item = resultado[0].itens.filter(function (table) {
          return table.imovel.id == id;
       })[0];
       console.log('item: ', this.item)
      });

    this.formGroup.get("imovel").setValue(this.formGroup.value.imovel);
/*     console.log('lista', lista._value);
    let imovel = lista.itens.filter(function (table) {
                
      return table.imovel.id == this.listaId;
   });
   console.log('imovel', imovel); */
  }

  salvar(event){

    console.log('dado a ser salvo', event);
    this.item.reconhecimento.conteudo = event;
    this.listaLocalService.getByListaId(this.item.lista.id, null, null, null, null)
      .then((value : ListaDeTrabalho[])=>{
        console.log('Esse item',this.item);
        let item = this.item;
        value[0].itens = value[0].itens.map(function (data) {
                if(data.id.imovelId == item.id.imovelId && data.id.listaId == item.id.listaId)
                    {data = item;
                      data.status =data.reconhecimento.conteudo === null ? "nao preenchido" : "preenchido";
                    }
                return data;          
       })

       this.listaLocalService.update(value[0]).then(value =>{
        this.abrirSnackBar("Registro salvo com sucesso!", null, "green-snackbar")
        this.location.back();
       })
       console.log('lista atualizada: ',  value[0]);
    })
  }

  excluir(){
    this.salvar(null);
  }

  voltar(){
    this.location.back();
  }

  abrirSnackBar(message: string, action: string, classe: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [classe]
    });
  }
}
