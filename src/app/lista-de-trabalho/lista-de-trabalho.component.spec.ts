import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDeTrabalhoComponent } from './lista-de-trabalho.component';

describe('ListaDeTrabalhoComponent', () => {
  let component: ListaDeTrabalhoComponent;
  let fixture: ComponentFixture<ListaDeTrabalhoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaDeTrabalhoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDeTrabalhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
