import { element } from 'protractor';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { HttpListaDeTrabalhoService } from './service/http-lista-de-trabalho.service';
import ListaDeTrabalho from '../model/listaDeTrabalho';
import { merge, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { Page } from '../model/Page';
import { SelectionModel } from '@angular/cdk/collections';
import ItemLista from '../model/itemLista';
import { HttpItemListaService } from './service/http-item-lista.service';
import { ListaLocalService } from './service/lista-local-service.service';

@Component({
  selector: 'app-lista-de-trabalho-local',
  templateUrl: './lista-de-trabalho-local.component.html',
  styleUrls: ['./lista-de-trabalho.component.scss']
})
export class ListaDeTrabalhoLocalComponent implements OnInit {
  
  formGroup: FormGroup = this.formBuilder.group({

  });
  //Atributos para tabela 
  colunasTabela: string[] = ['select', 'listaDeTrabalho', 'download'];
  selectionModel = new SelectionModel<ListaDeTrabalho>(true, []);
  dragPosition = {x: 0, y: -300};
  listas: ListaDeTrabalho[] = [];
  listaCompleta = false;
  quantidadeRegistros = 0;
  carregandoResultados = true;
  tempoLimiteAtingido = false;

  filtro: string = "";
  tamanhoMinimoTextoFiltro: number = 2;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: any;

  constructor(private apiListaDeTrabalhoService: HttpListaDeTrabalhoService,
    private formBuilder: FormBuilder,
    private listaLocalService: ListaLocalService,
    private apiItemListaService: HttpItemListaService,
    private snackBar : MatSnackBar) {

  }

  ngOnInit() {
    
    this.configurarTabela();
    this.dataSource = new MatTableDataSource<ListaDeTrabalho>([]);
  }
  configurarTabela() {
    // Se o imovel muda a ordem na tabela, volta para primeira pagina
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.carregandoResultados = true;
          return this.listaLocalService.getAllWithPagination(
            this.filtro,
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize | 30
          );

        }),
        map((resposta: Page<ListaDeTrabalho>) => {
          this.carregandoResultados = false;
          this.tempoLimiteAtingido = false;
          this.quantidadeRegistros = resposta.totalElements;
          return resposta;
        }),
        catchError(() => {
          this.carregandoResultados = false;
          this.tempoLimiteAtingido = true;
          return observableOf([]);
        })
      ).subscribe(resposta => {
        return this.tratarSucessoAoPesquisar(resposta);
      });
  }
  tratarSucessoAoPesquisar(resposta) {
    
    if(resposta.length !== 0){
      if(this.estaCompleta(resposta))
        this.listaCompleta = true;
    }
    
    this.listas = resposta;
    this.exibirCheckbox();
  }
  aplicarFiltroTabela() {
    if (this.filtro.length > this.tamanhoMinimoTextoFiltro) {
      this.paginator.page.emit();
    } else {
      this.filtro = "";
    }

    this.filtrarListas();
  }

  limparFiltroTabela() {
    this.filtro = "";
    this.paginator.page.emit();

  }

  aoDigitarFiltroTabela(filtro: string) {
    this.filtro = filtro;
    if (this.filtro.length === 0) {
      this.filtro = "";
      this.paginator.page.emit();
    }
  }





  filtrarListas() {
    this.paginator.page.emit();
  }

  exibirCheckbox() {
    console.log("Cheguei")
    this.dataSource.data = this.listas;

    this.selectionModel = new SelectionModel<ListaDeTrabalho>(true, []);
  }

  /** Se o número de elementos selecionados corresponde ao número total de linhas. */
  todosElementosEstaoSelecionados() {

    const numeroElementosSelecionados = this.selectionModel.selected.length;
    const numeroTotalElementos = this.dataSource.data.length;
    return numeroElementosSelecionados === numeroTotalElementos;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.todosElementosEstaoSelecionados() ?
      this.selectionModel.clear() :
      this.dataSource.data.forEach(row => this.selectionModel.select(row));
  }

  removerListas() {
    this.selectionModel.selected.forEach(element => {
      console.log('element: ', element);
      this.listaLocalService.deleteById(element.id);
    });
    this.paginator.page.emit();
  
  }
  enviar(lista : ListaDeTrabalho){
    console.log('item a ser enviado', lista);

    if(this.estaCompleta2(lista)){
      this.apiItemListaService.enviarListaInteira(lista).subscribe(data =>{
        console.log("resposta: ",data);
        lista.status = 'enviado';
        this.listaLocalService.update(lista).then(data=>{
          this.abrirSnackBar("Itens enviados com sucesso. ", null, "green-snackbar")
        });
        
      })
    }
      
    
  }

  

  estaCompleta(lista : ListaDeTrabalho) : boolean{
    let retorno = true;
     
     
        lista[0].itens.forEach(element => {
          
      if(element.reconhecimento.conteudo == null || element.reconhecimento.conteudo == undefined )
        {
          retorno = false;}
    
      });
      
    return retorno;
  }
  estaCompleta2(lista : ListaDeTrabalho) : boolean{
    let retorno = true;
     
     
        lista.itens.forEach(element => {
          
      if(element.reconhecimento.conteudo == null || element.reconhecimento.conteudo == undefined )
        {
          retorno = false;}
    
      });
      
    return retorno;
  }
  removerTodas(){
    this.listaLocalService.deleteAll();
    this.paginator.page.emit();
  }
 
abrirSnackBar(message: string, action: string, classe: string) {
  this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [classe]
  });
}
}
