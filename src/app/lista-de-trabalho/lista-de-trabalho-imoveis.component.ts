import { element } from 'protractor';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { HttpListaDeTrabalhoService } from './service/http-lista-de-trabalho.service';
import ListaDeTrabalho from '../model/listaDeTrabalho';
import { merge, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { Page } from '../model/Page';
import { SelectionModel } from '@angular/cdk/collections';
import ItemLista from '../model/itemLista';
import { HttpItemListaService } from './service/http-item-lista.service';
import { ListaLocalService } from './service/lista-local-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-de-trabalho-imoveis',
  templateUrl: './lista-de-trabalho-imoveis.component.html',
  styleUrls: ['./lista-de-trabalho.component.scss']
})
export class ListaDeTrabalhoImoveisComponent implements OnInit {
  //Atributos para selecao de filtro por imovel Pai
  formGroup: FormGroup = this.formBuilder.group({

  });

  //Atributos para tabela
  colunasTabela: string[] = ['imoveis', 'upload'];
 
  itens: ItemLista[];
  nomeLista : string;
  quantidadeRegistros = 0;
  carregandoResultados = true;
  tempoLimiteAtingido = false;
  id: number;
  filtro: string = "";
  tamanhoMinimoTextoFiltro: number = 2;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private listaLocalService: ListaLocalService,
    private apiItemListaService: HttpItemListaService,
    private snackBar : MatSnackBar) {
      this.activatedRoute.params.subscribe(params => this.id = params.id);
  }

  ngOnInit() {
    this.configurarTabela();
    this.dataSource = new MatTableDataSource<ItemLista>([]);
  }
  configurarTabela() {
    // Se o imovel muda a ordem na tabela, volta para primeira pagina
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.carregandoResultados = true;
          return this.listaLocalService.getByListaId(
            this.id,
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize | 30
          );

        }),
        map((resposta: ListaDeTrabalho[]) => {
          this.carregandoResultados = false;
          this.tempoLimiteAtingido = false;
          return resposta;
        }),
        catchError(() => {
          this.carregandoResultados = false;
          this.tempoLimiteAtingido = true;
          return observableOf([]);
        })
      ).subscribe(resposta => {
        return this.tratarSucessoAoPesquisar(resposta);
      });
  }
  tratarSucessoAoPesquisar(resposta) {
    console.log('dados da tabela: ', resposta);
    console.log('itens da resposta: ', resposta[0].itens);

    this.itens = resposta[0].itens;
    this.nomeLista = this.itens[0].lista.nome;  
    this.dataSource = resposta[0].itens;
  }
  aplicarFiltroTabela() {
    if (this.filtro.length > this.tamanhoMinimoTextoFiltro) {
      this.paginator.page.emit();
    } else {
      this.filtro = "";
    }

    this.filtrarListas();
  }

  limparFiltroTabela() {
    this.filtro = "";
    this.paginator.page.emit();

  }

  aoDigitarFiltroTabela(filtro: string) {
    this.filtro = filtro;
    if (this.filtro.length === 0) {
      this.paginator.page.emit();
    }
  }





  filtrarListas() {
    this.paginator.page.emit();
  }
  enviar(item : ItemLista){
    console.log('item a ser enviado', item);
    this.apiItemListaService.atualizarSemId(item).subscribe(data=>{
      item.status = "enviado";
      this.listaLocalService.updateItemLista(item);
      this.abrirSnackBar("Item enviado com sucesso. ", null, "green-snackbar")
    })

  }
  
abrirSnackBar(message: string, action: string, classe: string) {
  this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [classe]
  });
}
}
