

import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import ListaDeTrabalho from 'src/app/model/listaDeTrabalho';
import { DexieService } from 'src/app/service-generico/dexie.service';
import { Page } from 'src/app/model/Page';
import { Observable } from 'rxjs';
import ItemLista from 'src/app/model/itemLista';

@Injectable({
    providedIn: 'root'
})
export class ListaLocalService {
    table: Dexie.Table<ListaDeTrabalho, number>;
    constructor(private dexieService: DexieService) {
        this.table = this.dexieService.table("listas");
    }
    add(data: ListaDeTrabalho) {
        data.itens = data.itens.map(element => {
            element.reconhecimento.conteudo = null
            return element;
        });
        return this.table.add(<ListaDeTrabalho>data);
    }
    update(data: ListaDeTrabalho) {
        return this.table.update(data.id, <ListaDeTrabalho>data);
    }
    updateItemLista(item: ItemLista) {
        return this.getByListaId(item.lista.id, null, null, null, null)
            .then((value: ListaDeTrabalho[]) => {

                value[0].itens = value[0].itens.map(function (data) {
                    if (data.id.imovelId == item.id.imovelId && data.id.listaId == item.id.listaId) {
                        data = item;

                    }
                    return data;
                });

                return this.update(value[0]).then(value =>{
                    return true;
                })
            })


    }



    getAll() {

        return this.table.toArray();

    }

    deleteAll() {
        this.table.clear();
    }
    deleteById(id: number) {
        this.table.delete(id);
    }

    getAllWithPagination(
        name: string,
        orderby: string, sort: string,
        offset: number, limit: number) {

        console.log("AtividadesLocalService getAll", this.table)
        //TODO: Implementar ordenação. Há um problema de obter o total
        // Por exemplo no Spring, o total de registros já vem com a consulta automaticamente

        let whereClause;
        console.log('Nome: ', name)
        if (name) {
            whereClause = this.table.filter(function (table) {
                return table.nome.includes(name);
            })
        } else {
            whereClause = this.table;
        }


        /* if (orderby != undefined && sort != undefined) {

            if (sort == "asc") {
                return whereClause.orderBy(orderby).toArray();
            }

            return whereClause.orderBy(orderby).reverse().toArray();
        } */

        //console.log('resultado da consuta 2: ', whereClause.toArray());
        return whereClause.toArray();

    }

    getByListaId(
        listaId: number,
        orderby: string, sort: string,
        offset: number, limit: number): Promise<ListaDeTrabalho[]> {

        console.log("listaId", listaId)
        //TODO: Implementar ordenação. Há um problema de obter o total
        // Por exemplo no Spring, o total de registros já vem com a consulta automaticamente

        let whereClause;

        if (listaId) {
            whereClause = this.table.filter(function (table) {

                return table.id == listaId;
            })
        } else {
            whereClause = this.table;
        }


        /* if (orderby != undefined && sort != undefined) {

            if (sort == "asc") {
                return whereClause.orderBy(orderby).toArray();
            }

            return whereClause.orderBy(orderby).reverse().toArray();
        } */

        //console.log('resultado da consuta 2: ', whereClause.toArray());
        return whereClause.toArray();

    }
    getByListaIdAndItemId(
        listaId: number,
        itemId: number): Promise<ListaDeTrabalho[]> {


        //TODO: Implementar ordenação. Há um problema de obter o total
        // Por exemplo no Spring, o total de registros já vem com a consulta automaticamente

        let whereClause;

        if (listaId) {
            whereClause = this.table.filter(function (table) {

                return table.id == listaId;
            })
        } else {
            whereClause = this.table;
        }


        /* if (orderby != undefined && sort != undefined) {

            if (sort == "asc") {
                return whereClause.orderBy(orderby).toArray();
            }

            return whereClause.orderBy(orderby).reverse().toArray();
        } */

        //console.log('resultado da consuta 2: ', whereClause.toArray());
        return whereClause.toArray();

    }
    getImoveisByListaId(
        listaId: number,
        orderby: string, sort: string,
        offset: number, limit: number): Observable<ListaDeTrabalho[]> {

        console.log("listaId", listaId)
        //TODO: Implementar ordenação. Há um problema de obter o total
        // Por exemplo no Spring, o total de registros já vem com a consulta automaticamente

        let whereClause;

        if (listaId) {
            whereClause = this.table.filter(function (table) {

                return table.id == listaId;
            })
        } else {
            whereClause = this.table;
        }


        /* if (orderby != undefined && sort != undefined) {

            if (sort == "asc") {
                return whereClause.orderBy(orderby).toArray();
            }

            return whereClause.orderBy(orderby).reverse().toArray();
        } */

        //console.log('resultado da consuta 2: ', whereClause.toArray());
        return whereClause.toArray();

    }








}
