import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaDeTrabalhoComponent } from './lista-de-trabalho/lista-de-trabalho.component';
import { ListaDeTrabalhoImoveisComponent } from './lista-de-trabalho/lista-de-trabalho-imoveis.component';
import { ListaDeTrabalhoImoveisFormularioComponent } from './lista-de-trabalho/lista-de-trabalho-imoveis-formulario.component';


const routes: Routes = [ 
  {
  path: '',
  redirectTo: '/listas-de-trabalho',
  pathMatch: 'full'
},
{
  path : "listas-de-trabalho",
  component : ListaDeTrabalhoComponent
},
{
  path : "lista-de-trabalho-imoveis/:id",
  component : ListaDeTrabalhoImoveisComponent
},
{
  path : "lista-de-trabalho-imoveis-formulario/:id/:listaId",
  component : ListaDeTrabalhoImoveisFormularioComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
